# Порт перевода The Jackbox Party Starter от What If? на Nintendo Switch

### Как установить этот перевод?
Шаг 1. [Склонируйте данный репозиторий.](https://codeberg.org/JB-RU-NX/TJPS-WI/archive/main.zip)\
Шаг 2. Скопируйте папку `atmosphere` из папки `TJPS-WI` находящийся в архиве `TJPS-WI-main.zip` в корень вашей SD карты.\
Шаг 3. Запустить The Jackbox Party Starter и наслаждать :)